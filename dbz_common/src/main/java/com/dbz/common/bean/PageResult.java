package com.dbz.common.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author GL.Lian
 * @description: 该类用于..分页数据返回格式
 * @date: 2020/3/10 13:33
 * {
 *   "success": true,
 *   "code": 20000,
 *   "message": OK,
 *   "data": {
 *              total: //总条数
 *              rows: //数据列表
 *            }
 *   }
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageResult<T> {
    private Long total;
    private List<T> rows;
}
