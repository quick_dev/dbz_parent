package com.dbz.common.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author GL.Lian
 * @description: 数据相应对象 该类用于..规定接口返回的数据格式
 * @date: 2020/3/10 13:24
 * {
 *   "success": true,
 *   "code": 20000,
 *   "message": OK,
 *   "data":{
 *           返回数据
 *          }
 * }
 */
@Data
@NoArgsConstructor
public class Result {

    private boolean success;//是否成功
    private Integer code;// 返回码
    private String message;//返回信息
    private Object data;// 返回数据

    public Result(ResultCode code) {
        this.success = code.success;
        this.code = code.code;
        this.message = code.message;
    }

    public Result(ResultCode code, Object data) {
        this.success = code.success;
        this.code = code.code;
        this.message = code.message;
        this.data = data;
    }

    public Result(Integer code, String message, boolean success) {
        this.code = code;
        this.message = message;
        this.success = success;
    }

    public static Result SUCCESS() {
        return new Result(ResultCode.SUCCESS);
    }

    public static Result ERROR() {
        return new Result(ResultCode.SERVER_ERROR);
    }

    public static Result FAIL() {
        return new Result(ResultCode.FAIL);
    }
}