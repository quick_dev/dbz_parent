package com.dbz.common.exception;

import com.dbz.common.bean.ResultCode;
import lombok.Getter;

/**
 * @description: 自定义异常..
 * @date: 2020/3/12 9:36
 * @author: GL.Lian
 */

@Getter
public class CommonException extends Exception {

    private ResultCode resultCode;

    public CommonException(ResultCode resultCode) {
        this.resultCode = resultCode;
    }
}
