package com.dbz.common.service;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 * @author GL.Lian
 * @description: 该类用于..
 * @date: 2020/3/11 11:09
 */

public class BaseService<T> {

    /**
     * 构造查询条件: 返回的结果是一个查询条件
     * 1.只查询companyId
     * 2.很多的地方都需要根据companyId查询
     * 3.很多的对象中都具有companyId
     */
    protected Specification<T> getSpec(String companyId) {
        Specification<T> spect = new Specification() {
            /**
             * 构造查询条件
             *      root   ：包含了所有的对象数据
             *      cq     ：一般不用
             *      cb     ：构造查询条件
             */
            @Override
            public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder cb) {
                //根据企业id查询
                // where companyId = companyId
                return cb.equal(root.get("companyId").as(String.class), companyId);
            }
        };
        return spect;
    }
}
