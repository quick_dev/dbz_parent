package com.dbz.jwt;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

/**
 * @description: 该类用于..
 * @date: 2020/3/13 11:28
 * @author: GL.Lian
 */

public class CreateJwtTest {
    /*
     * 通过JWT创建一个token字符串
     */
    public static void main(String[] args) {

        JwtBuilder jwtBuilder = Jwts.builder().setId("dbz_common")
                .setSubject("包子")
                .setIssuedAt(new Date())
                .signWith(SignatureAlgorithm.HS256, "test_dbz");
        String token = jwtBuilder.compact();
        System.out.println(token);

    }
}
