package com.dbz.jwt;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

/**
 * 解析Token
 */

public class ParseJwtTest {
    public static void main(String[] args) {
        String token = "" +
                "eyJhbGciOiJIUzI1NiJ9." +
                "eyJqdGkiOiJkYnpfY29tbW9uIiwic3ViIjoi5YyF5a2QIiwiaWF0IjoxNTg0MDgwODU2fQ." +
                "VBeYww2QFfrJhwxE_jsiYezwNGp3RizQuZDOK6j4slg";
        Claims claims = Jwts.parser().setSigningKey("test_dbz").parseClaimsJws(token).getBody();
        //私有数据存在claims对象中
        System.out.println(claims.getId());
        System.out.println(claims.getIssuedAt());
        System.out.println(claims.getSubject());
    }
}
