package com.dbz.bean.company.response;

import com.dbz.bean.company.Company;
import com.dbz.bean.company.Department;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * @author GL.Lian
 * @description: 该类用于..构造返回结果..作用类似于Map但是可以避免代码重复
 * @date: 2020/3/11 10:41
 */

@Getter
@Setter
@NoArgsConstructor
public class DeptListResult {

    private String companyId;
    private String companyName;
    private String companyManage;//公司联系人
    private List<Department> depts;

    public DeptListResult(Company company, List depts) {
        this.companyId = company.getId();
        this.companyName = company.getName();
        this.companyManage = company.getLegalRepresentative();//公司联系人
        this.depts = depts;
    }
}
