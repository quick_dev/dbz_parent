package com.dbz.bean.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "pe_permission")
@Getter
@Setter
@NoArgsConstructor
@DynamicInsert(true)
@DynamicUpdate(true)
public class Permission implements Serializable {
    private static final long serialVersionUID = -90000080L;
    /**
     * 主键
     */
    @Id
    private String id;
    /**
     * 权限名称 (根据权限类型定: 菜单:菜单名称/ 功能:功能名称/  API:API名称)
     */
    private String name;
    /**
     * 权限类型 1为菜单 2为功能 3为API
     */
    private Integer type;

    /**
     * 权限编码(根据此标志来判断用户是否有权访问)
     */
    private String code;

    /**
     * 权限描述
     */
    private String description;

    /**
     * pid: 父id
     * 用来描述权限的所属关系
     *      菜单   ---   多个按钮
     *      菜单   ---   多个api
     *      按钮   ---   api权限
     * 默认值: "0"
     */
    private String pid;

    private Integer enVisible;

    public Permission(String name, Integer type, String code, String description) {
        this.name = name;
        this.type = type;
        this.code = code;
        this.description = description;
    }


}