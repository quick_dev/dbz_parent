package com.dbz.bean.system.response;

import com.dbz.bean.system.Role;
import com.dbz.bean.system.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 * 返回用户信息: 带着用户的角色信息(roleIds)
 */
@Getter
@Setter
public class UserResult implements Serializable {

    /**
     * ID
     */
    @Id
    private String id;
    /**
     * 手机号码
     */
    private String mobile;
    /**
     * 用户名称
     */
    private String username;
    /**
     * 密码
     */
    private String password;

    /**
     * 启用状态 0为禁用 1为启用
     */
    private Integer enableState;
    /**
     * 创建时间
     */
    private Date createTime;

    private String companyId;

    private String companyName;

    /**
     * 部门ID
     */
    private String departmentId;

    /**
     * 入职时间
     */
    private Date timeOfEntry;

    /**
     * 聘用形式
     */
    private Integer formOfEmployment;

    /**
     * 工号
     */
    private String workNumber;

    /**
     * 管理形式
     */
    private String formOfManagement;

    /**
     * 工作城市
     */
    private String workingCity;

    /**
     * 转正时间
     */
    private Date correctionTime;

    /**
     * 在职状态 1.在职  2.离职
     */
    private Integer inServiceStatus;

    private String departmentName;

    private List<String> roleIds = new ArrayList<>();

    private String staffPhoto;//用户头像

    /*
     * BeanUtils.copyProperties(user, this)
     * 拷贝user的属性到当前对象
     * (把原对象中的List<Role>  --> List<String>)
     * 把Role 变成了 roleIds数组 方便前端对该用户的已分配权限进行回显
     */
    public UserResult(User user) {
        BeanUtils.copyProperties(user, this);
        for (Role role : user.getRoles()) {
            this.roleIds.add(role.getId());
        }
    }
}
