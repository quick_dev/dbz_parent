package com.dbz.company;

import com.dbz.common.utils.IdWorker;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;

//1.配置springboot的包扫描
@SpringBootApplication(scanBasePackages = "com.dbz")
//2.配置jpa注解的扫描
@EntityScan(value = "com.dbz.bean.company")
public class CompanyApplication {

    /**
     * 启动方法
     */
    public static void main(String[] args) {
        SpringApplication.run(CompanyApplication.class, args);
    }

    /**
     * 将id生成器配置到当前工程
     */
    @Bean
    public IdWorker idWorker() {
        return new IdWorker();
    }
}
