package com.dbz.company.service;

import com.dbz.bean.company.Company;
import com.dbz.common.utils.IdWorker;
import com.dbz.company.dao.CompanyDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author GL.Lian
 * @description: 该类用于..
 * @date: 2020/3/10 17:21
 */

@Service
public class CompanyService {

    @Autowired
    private CompanyDao companyDao;

    @Autowired
    private IdWorker idWorker;

    /**
     * 保存企业
     * 1、配置Idwork到工程中
     * 2、在service中注入idwork
     * 3、通过idwork生成id
     * 4、保存企业
     */
    public void add(Company company) {
        //基本属性的设置
        String id = String.valueOf(idWorker.nextId());// long转String
        company.setId(id);
        //默认的状态
        company.setAuditState("0"); //0未审核，1已审核
        company.setState(1); //0未激活，1已激活
        companyDao.save(company);//save 有则更新,无则插入
    }


    /**
     * 更新企业
     * 1、参数，Company
     * 2、根据id查询企业对象
     * 3、设置修改的属性值
     * 4、调用dao完成更新
     */
    public void update(Company company) {
        Company temp = companyDao.findById(company.getId()).get();
        temp.setName(company.getName());
        temp.setCompanyPhone(company.getCompanyPhone());
        companyDao.save(temp);
    }

    /**
     * 删除企业
     */
    public void deleteById(String id) {
        companyDao.deleteById(id);
    }

    /**
     * 根据id查询企业
     */
    public Company findById(String id) {
        return companyDao.findById(id).get();
    }

    /**
     * 查询全部企业列表
     */
    public List<Company> findAll() {
        return companyDao.findAll();
    }

}
