package com.dbz.system;

import com.dbz.common.utils.IdWorker;
import com.dbz.common.utils.JwtUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;

/**
 * @author GL.Lian
 * @description: 该类用于..
 * @date: 2020/3/11 15:15
 */
//1.配置springboot的包扫描
@SpringBootApplication(scanBasePackages = "com.dbz")
//2.配置jpa注解的扫描
@EntityScan(value = "com.dbz.bean.system")
public class SystemApplication {
    /**
     * 启动方法
     */
    public static void main(String[] args) {
        SpringApplication.run(SystemApplication.class, args);
    }

    /**
     * 将id生成器配置到当前工程
     */
    @Bean
    public IdWorker idWorker() {
        return new IdWorker();
    }

    /*
     * 解决no session
     */
    @Bean
    public OpenEntityManagerInViewFilter openEntityManagerInViewFilter() {
        return new OpenEntityManagerInViewFilter();
    }

}
